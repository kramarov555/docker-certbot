FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y software-properties-common 
RUN add-apt-repository ppa:certbot/certbot
RUN apt-get update
RUN apt-get install -y python-certbot-nginx
COPY ./run.sh /run.sh

RUN chmod +x /run.sh

CMD ["/bin/sh",  "-c", "/run.sh"]
